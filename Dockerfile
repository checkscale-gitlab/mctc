FROM ubuntu:20.04

# update packages 
RUN  apt update -y \
&&  apt upgrade -y
# Set the locale
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -y install tzdata
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
# install locales 
RUN apt install locales
RUN locale-gen
# install software 
COPY utils/scripts/apt_packages_to_install.txt .
RUN xargs apt install -y < apt_packages_to_install.txt
# cleanup packages 
RUN apt autoremove -y && apt clean -y \
&& rm -rf /var/cache/apt/archives/

# Create a user with passwordless sudo
ARG USERNAME=user
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# setup user space
USER $USERNAME
WORKDIR /home/user
# copy files into the container 
COPY --chown=$USERNAME:$USERNAME . .
RUN ["/bin/bash", "-c", "./utils/scripts/install_omb.sh"]
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/bash .


CMD echo "starting container" && tail -f /dev/null 